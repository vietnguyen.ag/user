package helper

import "testing"

func TestRemoveSubString(t *testing.T) {
	tests := []struct {
		name string
		s    string
		sep  string
		n    int
		want string
	}{
		{name: "case 1", s: "10002,332452", sep: ",", n: 3, want: "10002,332"},
		{name: "case 2", s: "10002,3", sep: ",", n: 3, want: "10002,3"},
		{name: "case 3", s: "10002", sep: ",", n: 3, want: "10002"},
		{name: "case 4", s: "10002,123", sep: ",", n: 3, want: "10002,123"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RemoveSubString(tt.s, tt.sep, tt.n); got != tt.want {
				t.Errorf("RemoveSubString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMultyCases(t *testing.T) {
	cases := []struct {
		name   string
		input  string
		output string
	}{
		{
			name:   "Case A",
			input:  "a",
			output: "A",
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			if got := MultyCases(tt.input); got != tt.output {
				t.Errorf("MultyCases() = %v, want %v", got, tt.output)
			}
		})
	}
}
