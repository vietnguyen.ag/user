package helper

import "strings"

func RemoveSubString(s, sep string, n int) string {
	ss := strings.Split(s, sep)
	if len(ss) <= 1 {
		return s
	}

	if len(ss[1]) > n {
		return s[:len(s)-(len(ss[1])-n)]
	}

	return s
}

func MultyCases(val string) string {
	if val == "a" {
		return "A"
	}

	if val == "b" {
		return "B"
	}

	if val == "c" {
		return "C"
	}

	return val
}
