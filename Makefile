GOCOVER_COBERTURA = gocover-cobertura

.PHONY: test coverage

test: ## Run unittests
	@go test -race ${PKG_LIST}

#msan: dep ## Run memory sanitizer
# @go test -msan -short ${UNITTEST_PKG_LIST}

#coverage: ## Generate global code coverage report
	#./scripts/coverage.sh;

test-coverage:
	go test -coverprofile=coverage.out -covermode count ./...
	go tool cover -func=coverage.out

cobertura-report: $(GOCOVER_COBERTURA)
	echo "Begin generate coverage.xml"
	$(GOCOVER_COBERTURA) < coverage.out > coverage.xml
	echo "Finish generate coverage.xml"

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

# development tools
$(GOCOVER_COBERTURA):
	go install github.com/boumenot/gocover-cobertura@latest
